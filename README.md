# Taller de Programación 1

Bienvenidos al repositorio oficial del curso "Taller de Programación 1". Aquí encontrarán todos los recursos, fechas y temarios relacionados con el curso.

## Fechas de Evaluación


- **Entregable 1**: 7 de Septiembre, 2023

- **Entregable 2**: 28 de Septiembre, 2023

- **Parcial 1**: 5 de Octubre, 2023

- **Entregable 3**: 2 de Noviembre, 2023

- **Entregable 4**: 30 de Noviembre, 2023

- **Parcial Final**: 7 de Diciembre, 2023


## Herramientas Utilizada

- **Git**: [Sitio oficial](https://git-scm.com/)
- **Visual Studio Code**: [Descargar aquí](https://code.visualstudio.com/)Extenciones (Python , Git grafh , Git lens)


## Control de Versiones

- **git clone** 
- **git add** .  Empaquetar cambios
- **git commit -m "descripcion del cambio"** Etiquetar version
- **git push origin main** Carga o empuja el paquete a la ubicacion remota

## Espacio de Trabajo

**python -m venv venv creacion de espacio de trabajo**

**.\venv\Scripts\activate activar entorno virtual**

## Comandos VSCode

- Control + ñ Terminal

## librerias

**1. Comandos de PIP**



**item**
**comando**
**Documentación y detalle**




**1**
**pip install**
**base inicial para instalar cualquier libreria comando completo**


**2**
**pip list**
**lista las libreria instaladas**
## Sesiones

|Sesión | Fecha      | Tema                         |
|-------|------------|------------------------------|
| 1     | 09/08/2023 |Introduccion a la programacion|
| 1     | 10/08/2023 |Control de Versiones          |
