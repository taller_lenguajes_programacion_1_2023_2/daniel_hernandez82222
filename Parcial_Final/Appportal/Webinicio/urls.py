from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('producto-detalle/<int:producto_id>/', views.producto_detalle, name='producto-detalle'),
    path('profesionales/', views.profesionales_view, name='profesionales'),
    path('no-profesionales/', views.no_profesional_view, name='no_profesionales'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('perfil/', views.perfil, name='perfil'),
    path('ver-carrito/', views.ver_carrito, name='ver_carrito'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('registro/', views.registro, name='registro'),
]