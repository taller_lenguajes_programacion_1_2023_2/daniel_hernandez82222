function agregarAlCarrito(nombreProducto, precioProducto) {
    let producto = { nombre: nombreProducto, precio: precioProducto };
    let carrito = JSON.parse(localStorage.getItem('carrito')) || [];

    carrito.push(producto);
    localStorage.setItem('carrito', JSON.stringify(carrito));
}

$(document).ready(function() {
    function actualizarCarrito(carrito) {
      let total = 0;
      let tablaCarrito = $('#tablaCarrito');
      tablaCarrito.empty();

      carrito.forEach((producto, index) => {
        let fila = $('<tr>');
        fila.html(`
          <td>${producto.nombre}</td>
          <td>$${producto.precio}</td>
          <td><button class="eliminar" data-index="${index}">Eliminar</button></td>
        `);
        tablaCarrito.append(fila);
        total += producto.precio;
      });

      $('#totalPrecio').text(`$${total}`);
        $('.eliminar').on('click', function() {
            let index = $(this).data('index');
            carrito.splice(index, 1);
            localStorage.setItem('carrito', JSON.stringify(carrito));
            actualizarCarrito(carrito);
            });
    }

    let carrito = JSON.parse(localStorage.getItem('carrito')) || [];
    actualizarCarrito(carrito);
  });