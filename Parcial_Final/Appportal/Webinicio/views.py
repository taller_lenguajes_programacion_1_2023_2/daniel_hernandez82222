from .models import Producto, Carrito, Banner
from .forms import RegistroForm
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect
from django.contrib.auth import login, logout

def index(request):
    productos = Producto.objects.all()
    banners = Banner.objects.all()
    return render(request, 'index.html', {'productos': productos, 'banners': banners})

def producto_detalle(request, producto_id):
    producto = Producto.objects.get(id=producto_id)
    imagenes = producto.productoimagen_set.all()
    return render(request, 'producto_detalle.html', {'producto': producto, 'imagenes': imagenes})

def profesionales_view(request):
    productos_profesionales = Producto.objects.filter(categoria='profesional')
    return render(request, 'profesional.html', {'productos': productos_profesionales})

def no_profesional_view(request):
    productos_no_profesionales = Producto.objects.filter(categoria='no_profesional')
    return render(request, 'no_profesional.html', {'productos': productos_no_profesionales})

def iniciar_sesion(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('index')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

@login_required
def perfil(request):
    usuario = request.user
    return render(request, 'perfil.html', {'usuario': usuario})

def cerrar_sesion(request):
    logout(request)
    return redirect('index')

def registro(request):
    if request.method == 'POST':
        form = RegistroForm(request.POST)
        if form.is_valid():
            user = form.save()
            return redirect('login')
    else:
        form = RegistroForm()
    return render(request, 'registro.html', {'form': form})

def ver_carrito(request):
    productos = Producto.objects.all()
    return render(request, 'carrito.html', {'productos': productos})
