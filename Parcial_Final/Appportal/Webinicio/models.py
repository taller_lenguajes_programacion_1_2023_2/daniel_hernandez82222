from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ProductoImagen(models.Model):
  producto = models.ForeignKey('Producto', on_delete=models.CASCADE)
  imagen = models.ImageField(upload_to='producto_imagenes/')
  
class Producto(models.Model):
  nombre = models.CharField(max_length=255)
  precio = models.DecimalField(max_digits=10, decimal_places=2)
  imagen = models.ImageField(upload_to="productos", null=True)
  categoria = models.CharField(max_length=255, choices=[('profesional', 'Profesional'), ('no_profesional', 'No profesional')])
  descripcion = models.TextField(blank=True, null=True)
  
  def __str__(self):
    return self.nombre
  
  def get_imagen_url(self):
    return self.imagen.url if self.imagen else ''

class Carrito(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    productos = models.ManyToManyField(Producto)
    
    from django.db import models

class Banner(models.Model):
    nombre = models.CharField(max_length=255)
    imagen = models.ImageField(upload_to='banners/')

    def __str__(self):
        return self.nombre