from django.contrib import admin
from .models import Producto, ProductoImagen, Banner

class ProductoImagenInline(admin.TabularInline):
    model = ProductoImagen
    extra = 1

@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'precio', 'get_imagen_url')  # Utiliza la función definida en el modelo Producto para mostrar la imagen
    inlines = [ProductoImagenInline]  # Añade el inline de ProductoImagen

    def get_imagen_url(self, obj):
        return obj.get_imagen_url()  # Función para obtener la URL de la imagen
    get_imagen_url.short_description = 'Imagen'  # Cambia el encabezado de la columna en el admin

@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'imagen')