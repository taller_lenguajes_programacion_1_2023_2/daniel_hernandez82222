import tkinter as tk
from tkinter import messagebox, ttk
from Cancion import Cancion
from Artista import Artista

class Ejecucion:
    
    """
      Esta clase define la interfaz gráfica para gestionar una base de datos de canciones.
    
       Attributes:
       ventanaPrincipal: La ventana principal de la aplicación.
       frameDatosCancion: El marco que contiene los campos de entrada de datos de canción.
       frameBotonesSuperior: El marco que contiene los botones superiores (Nuevo, Guardar, Cancelar).
       frameBaseDeDatos: El marco que muestra la tabla de datos de canciones.
       frameBotonesLateral: El marco que contiene los campos de entrada de ID y los botones Editar y Eliminar.
       cancion: Una instancia de la clase Cancion utilizada para interactuar con la base de datos de canciones.
    """
    def __init__(self):
        """
        Inicializa la ventana principal y configura la interfaz gráfica.
        Crea marcos para diferentes secciones y agrega campos de entrada, botones y una tabla.
        """
        self.ventanaPrincipal = tk.Tk()
        self.ventanaPrincipal.title("Base de datos canciones")
        self.ventanaPrincipal.resizable(False, False)
        self.frameDatosCancion = tk.Frame(self.ventanaPrincipal)
        self.frameDatosCancion.grid(row=0, column=0, padx=5, pady=5)
        self.frameBaseDeDatosArtistas = tk.Frame(self.ventanaPrincipal)
        self.frameBaseDeDatosArtistas.grid(row=3, column=0, padx=5, pady=5)
        self.frameDatosArtista = tk.Frame(self.ventanaPrincipal)
        self.frameDatosArtista.grid(row=3, column=0, padx=5, pady=5)
        self.tablaBaseDeDatosArtistas = ttk.Treeview(self.frameBaseDeDatosArtistas, show="headings")
        self.frameBotonesSuperior = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesSuperior.grid(row=1, column=0, padx=5, pady=5)
        self.frameBaseDeDatos = tk.Frame(self.ventanaPrincipal)
        self.frameBaseDeDatos.grid(row=2, column=0, padx=5, pady=5)
        self.frameBotonesLateral = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesLateral.grid(row=2, column=1, padx=5, pady=10)
        self.cancion = Cancion()
        self.artista = Artista()
        self.camposDeTexto()
        self.botonesArriba()
        self.crearTabla()
        self.crearTablaArtistas()
        self.vincularDB()
        self.vincularDBArtistas()
        self.botonesLateral()
        self.ventanaPrincipal.mainloop()

    def crearTabla(self):
        """Crea la tabla de visualización de datos de canciones en la interfaz."""

        self.tablaBaseDeDatos = ttk.Treeview(self.frameBaseDeDatos, show="headings")
        self.tablaBaseDeDatos.config(columns=("ID", "Título", "Artista", "ID Artista"))
        self.tablaBaseDeDatos.heading("ID", text="ID")
        self.tablaBaseDeDatos.heading("Título", text="TÍTULO")
        self.tablaBaseDeDatos.heading("Artista", text="ARTISTA")
        self.tablaBaseDeDatos.heading("ID Artista", text="ID ARTISTA")
        self.tablaBaseDeDatos.grid(row=0, column=0)
        
    def crearTablaArtistas(self):
        """Crea la tabla de visualización de datos de artistas en la interfaz."""
        self.tablaBaseDeDatosArtistas = ttk.Treeview(self.frameBaseDeDatosArtistas, show="headings")
        self.tablaBaseDeDatosArtistas.config(columns=("ID", "name", "genero"))
        self.tablaBaseDeDatosArtistas.heading("ID", text="ID")
        self.tablaBaseDeDatosArtistas.heading("name", text="NOMBRE")
        self.tablaBaseDeDatosArtistas.heading("genero", text="GÉNERO")
        self.tablaBaseDeDatosArtistas.grid(row=0, column=0)


    def camposDeTexto(self):
        """Crea los campos de entrada para Título, Artista e ID del Artista en la interfaz."""

        self.variableTitulo = tk.StringVar()
        self.textoTitulo = tk.Label(self.frameDatosCancion, text="Título: ")
        self.textoTitulo.grid(row=0, column=0)
        self.cuadroTitulo = tk.Entry(self.frameDatosCancion, textvariable=self.variableTitulo)
        self.cuadroTitulo.grid(row=0, column=1)

        self.variableArtista = tk.StringVar()
        self.textoArtista = tk.Label(self.frameDatosCancion, text="Artista: ")
        self.textoArtista.grid(row=1, column=0)
        self.cuadroArtista = tk.Entry(self.frameDatosCancion, textvariable=self.variableArtista)
        self.cuadroArtista.grid(row=1, column=1)

        self.variableIDArtista = tk.StringVar()
        self.textoIDArtista = tk.Label(self.frameDatosCancion, text="ID Artista: ")
        self.textoIDArtista.grid(row=2, column=0)
        self.cuadroIDArtista = tk.Entry(self.frameDatosCancion, textvariable=self.variableIDArtista)
        self.cuadroIDArtista.grid(row=2, column=1)
        
        self.variableGenero = tk.StringVar()
        self.textoGenero = tk.Label(self.frameDatosCancion, text="Género: ")
        self.textoGenero.grid(row=3, column=0)
        self.cuadroGenero = tk.Entry(self.frameDatosCancion, textvariable=self.variableGenero)
        self.cuadroGenero.grid(row=3, column=1)
        
        

    def botonesArriba(self):
        """Crea los botones superiores: Nuevo, Guardar y Cancelar."""
        self.botonNuevo = tk.Button(self.frameBotonesSuperior, text="Nueva Canción", command=self.Nuevo)
        self.botonNuevo.grid(row=0, column=0, padx=5)

        self.botonGuardarCancion = tk.Button(self.frameBotonesSuperior, text="Guardar Canción", command=self.Guardar)
        self.botonGuardarCancion.grid(row=0, column=1, padx=5)

        self.botonCancelar = tk.Button(self.frameBotonesSuperior, text="Cancelar", command=self.Cancelar)
        self.botonCancelar.grid(row=0, column=2, padx=5)
        
        self.botonNuevoArtista = tk.Button(self.frameBotonesSuperior, text="Nuevo Artista", command=self.NuevoArtista)
        self.botonNuevoArtista.grid(row=1, column=0, padx=5)
        
        self.botonGuardarArtista = tk.Button(self.frameBotonesSuperior, text="Guardar Artista", command=self.agregarArtista)
        self.botonGuardarArtista.grid(row=1, column=1, padx=5)
        
        self.botonVerCanciones = tk.Button(self.frameBotonesSuperior, text="Ver Canciones", command=self.verCanciones)
        self.botonVerCanciones.grid(row=2, column=0, padx=5)
        
        self.botonVerArtistas = tk.Button(self.frameBotonesSuperior, text="Ver Artistas", command=self.verArtistas)
        self.botonVerArtistas.grid(row=2, column=1, padx=5)
        self.botonGuardarCambios = tk.Button(self.frameBotonesSuperior, text="Guardar Cambios", command=self.guardarCambios)
        self.botonGuardarCambios.grid(row=2, column=2, padx=5)

    def verCanciones(self):
        self.Cancion = Cancion()  
        self.Cancion.verTabla()
        self.vincularDB()
    
    def verArtistas(self):
        artista = Artista()
        contenido = artista.seleccionarTabla("artistas")
        self.tablaBaseDeDatosArtistas.delete(*self.tablaBaseDeDatosArtistas.get_children())
        for fila in contenido:
            self.tablaBaseDeDatosArtistas.insert("", "end", values=fila)
    
    def Nuevo(self):
        """Maneja la acción de crear una nueva canción en la base de datos o borrar los campos de entrada."""
        self.variableTitulo.set('')
        self.variableArtista.set('')
        self.variableIDArtista.set('')
        self.variableGenero.set('')
        self.variableID.set(0)
        self.variableIDArt.set(0)
        self.cuadroTitulo.config(state='normal')
        self.cuadroArtista.config(state='normal')
        self.cuadroIDArtista.config(state='normal')
        self.botonGuardarCancion.config(state='normal')
        self.botonGuardarArtista.config(state='normal')
        self.botonCancelar.config(state='normal')
        
        
    def agregarArtista(self):
        name = self.variableArtista.get()
        genero = self.variableGenero.get()
        
        if name and genero:
                self.artista.nombre = name
                self.artista.genero = genero
                self.artista.agregarDato()
                self.variableArtista.set('')
                self.variableGenero.set('')

                self.vincularDBArtistas()
                self.variableIDArtista.set('')
        else:
                messagebox.showerror("Error", "Debe ingresar el nombre y género musical del artista.")


                
    def NuevoArtista(self):
        """Borrar campos para ingresar nuevo artista."""
        self.cuadroArtista.config(state='normal')
        self.botonGuardarArtista.config(state='normal')
        self.cuadroGenero.config(state='normal')
        self.cuadroIDArtista.config(state='normal')
        self.botonCancelar.config(state='normal')
        self.variableArtista.set('')
        self.variableIDArtista.set('')
        self.variableGenero.set('')

    def vincularTablaArtistas(self):
        self.artista = Artista()
        contenido = self.artista.seleccionarTabla("artistas")
        self.tablaBaseDeDatosArtistas.delete(*self.tablaBaseDeDatosArtistas.get_children())
        for fila in contenido:
            self.tablaBaseDeDatosArtistas.insert("", "end", values=fila)
            
    def vincularDBArtistas(self):
        self.artista.verTabla()
        self.tablaBaseDeDatosArtistas.delete(*self.tablaBaseDeDatosArtistas.get_children())
        for fila in self.artista.datosEnLaTabla:
            self.tablaBaseDeDatosArtistas.insert("", "end", values=fila)

    def Cancelar(self):
        """Cancela la edición de una canción y desactiva los campos de entrada."""
        self.variableTitulo.set('')
        self.variableArtista.set('')
        self.variableIDArtista.set('')
        self.cuadroTitulo.config(state='disabled')
        self.cuadroArtista.config(state='disabled')
        self.cuadroIDArtista.config(state='disabled')
        self.botonGuardarCancion.config(state='disabled')
        self.botonGuardarArtista.config(state='disabled')
        self.botonCancelar.config(state='disabled')
        self.cuadroGenero.config(state='disabled')
        self.botonGuardarCambios.config(state='disabled')
        self.variableID.set(0)
        self.variableIDArt.set(0)
        
    def guardarCambios(self):
        """
        Guarda los datos de un nuevo artista en la base de datos o actualiza un artista existente.
        También desactiva los campos de entrada.
        """
        self.id_artista = self.variableID.get()
        if not self.id_artista:
                self.artista.nombre = self.variableArtista.get()
                self.artista.genero = self.variableGenero.get()
                self.artista.agregarDato()

                self.variableArtista.set('')
                self.variableGenero.set('')
        else:
                self.artista.nombre = self.variableArtista.get()
                self.artista.genero = self.variableGenero.get()

                self.artista.actualizarDatos("Artistas", "Nombre", self.artista.nombre, self.id_artista)
                self.artista.actualizarDatos("Artistas", "Genero", self.artista.genero, self.id_artista)

        self.cuadroArtista.config(state='disabled')
        self.cuadroGenero.config(state='disabled')
        self.botonGuardarArtista.config(state='disabled')
        self.botonGuardarCambios.config(state='disabled')
        self.vincularDBArtistas()

    def Guardar(self):
        """
        Guarda los datos de una nueva canción en la base de datos o actualiza una canción existente.
        También desactiva los campos de entrada.
        """
        self.id_cancion = self.variableID.get()      
        if not self.id_cancion:
                self.cancion.titulo = self.variableTitulo.get()
                self.cancion.artista = self.variableArtista.get()
                self.cancion.ID_Artista = self.variableIDArtista.get()
                self.cancion.agregarDato()

                self.variableTitulo.set('')
                self.variableArtista.set('')
                self.variableIDArtista.set('')
                self.variableGenero.set('')
        else:
                self.cancion.titulo = self.variableTitulo.get()
                self.cancion.artista = self.variableArtista.get()
                self.cancion.ID_Artista = self.variableIDArtista.get()
                self.cancion.actualizarDatos("Canciones", "Titulo", self.cancion.titulo, self.id_cancion)
                self.cancion.actualizarDatos("Canciones", "Artista", self.cancion.artista, self.id_cancion)
                self.cancion.actualizarDatos("Canciones", "ID_Artista", self.cancion.ID_Artista, self.  id_cancion)

        self.cuadroTitulo.config(state='disabled')
        self.cuadroArtista.config(state='disabled')
        self.cuadroIDArtista.config(state='disabled')
        self.botonGuardarCancion.config(state='disabled')
        self.cuadroGenero.config(state='disabled')
        self.vincularDB()

    def vincularDB(self):
        """Actualiza la tabla con los datos de canciones de la base de datos."""

        self.cancion.verTabla()
        self.tablaBaseDeDatos.delete(*self.tablaBaseDeDatos.get_children())
        for fila in self.cancion.datosEnLaTabla:
            self.tablaBaseDeDatos.insert("", "end", values=fila)

    def botonesLateral(self):
        """Crea los campos de entrada de ID y los botones Editar y Eliminar en el lateral de la interfaz."""
        self.variableID = tk.IntVar()
        self.textoID = tk.Label(self.frameBotonesLateral, text="ID Canción: ")
        self.textoID.grid(row=0, column=0, pady=5)
        self.cuadroID = tk.Entry(self.frameBotonesLateral, textvariable=self.variableID)
        self.cuadroID.grid(row=1, column=0)
        
        self.botonEditarCancion = tk.Button(self.frameBotonesLateral, text="Editar Canción", command=self.Editar)
        self.botonEditarCancion.grid(row=2, column=0, padx=5)
        self.botonEliminarCancion = tk.Button(self.frameBotonesLateral, text="Eliminar Canción", command=self.Eliminar)
        self.botonEliminarCancion.grid(row=3, column=0, padx=5)
        
        self.variableIDArt = tk.IntVar()
        self.textoID = tk.Label(self.frameBotonesLateral, text="ID Artista: ")
        self.textoID.grid(row=4, column=0, pady=5)
        self.cuadroIDArt = tk.Entry(self.frameBotonesLateral, textvariable=self.variableIDArt)
        self.cuadroIDArt.grid(row=5, column=0, pady=5)
        
        self.botonEditarArtistas = tk.Button(self.frameBotonesLateral, text="Editar Artista", command=self.EditarArtista)
        self.botonEditarArtistas.grid(row=6, column=0, padx=5)
        self.botonEliminarArtista = tk.Button(self.frameBotonesLateral, text="Eliminar Artista", command=self.EliminarArtista)
        self.botonEliminarArtista.grid(row=7, column=0, padx=5)

    def Editar(self):
        """Maneja la edición de una canción y habilita los campos de entrada para edición."""

        id_cancion = self.variableID.get()
        if id_cancion:
                song_data = self.cancion.obtenerDatoPorID(id_cancion)
                if song_data:
                        self.variableTitulo.set(song_data[1])
                        self.variableArtista.set(song_data[2])
                        self.variableIDArtista.set(song_data[3])

                        self.cuadroTitulo.config(state='normal')
                        self.cuadroArtista.config(state='normal')
                        self.cuadroIDArtista.config(state='normal')
                        self.botonGuardarCancion.config(state='normal')
                        self.botonCancelar.config(state='normal')
                        
        else:
                messagebox.showerror("Error", "Selecciona una canción para editar.")
                
    def EditarArtista(self):
        id_artista = self.variableIDArt.get()
        artist_data = self.artista.obtenerDatoPorID(id_artista,)

        if id_artista:
                artist_data = self.artista.obtenerDatoPorID(id_artista,)
                if artist_data:
                        self.variableArtista.set(artist_data[1])
                        self.variableGenero.set(artist_data[2])
                        self.cuadroTitulo.config(state='disabled')
                        self.cuadroArtista.config(state='normal')
                        self.cuadroIDArtista.config(state='disabled')
                        self.cuadroGenero.config(state='normal')
                        self.botonGuardarCancion.config(state='disabled')
                        self.botonCancelar.config(state='normal')
                        self.botonGuardarCambios.config(state='normal')
                        self.botonGuardarArtista.config(state='disabled')
                        self.vincularDBArtistas()
        else:
                messagebox.showerror("Error", "Debe ingresar un ID válido, nombre y género musical del artista.")



    def Eliminar(self):
        """Elimina una canción de la base de datos y reorganiza los IDs de canciones restantes."""
        id_cancion = self.variableID.get()
        if id_cancion is not None and id_cancion != "":
                self.cancion.eliminarDatos("Canciones", int(id_cancion))
                self.cancion.ajustarIDs(int(id_cancion))
                messagebox.showinfo("Base de datos canciones", "Se eliminaron los datos en la tabla.")
                self.vincularDB()
                self.variableID.set(0)
        else:
                messagebox.showerror("Error", "ID de canción no válido.")

    def EliminarArtista(self):
        id_artista = self.variableIDArt.get()
        if id_artista is not None and id_artista != "":
            self.artista.eliminarDato(int(id_artista,))
            self.vincularDBArtistas()
            messagebox.showinfo("Artista eliminado", "Se ha eliminado el artista correctamente.")
            self.variableIDArt.set(0)
        else:
            messagebox.showerror("Error", "ID de artista no válido.")


aplicacion = Ejecucion()
