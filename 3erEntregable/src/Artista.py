from Conexion import Conexion

class Artista(Conexion):
    """
    Clase que gestiona las operaciones relacionadas con la entidad de artistas en la base de datos SQLite.
    """

    def __init__(self, nombre="", genero=""):
        """
        Inicializa una instancia de la clase `Artista` con los atributos especificados.

        Args:
        - name (str): El nombre del artista (opcional).
        - genero (str): El género musical del artista (opcional).

        Esta clase hereda de la clase `Conexion` para gestionar la conexión a la base de datos.
        Crea la tabla 'artistas' si no existe y inicializa la lista de datos de la tabla.
        """
        super().__init__()
        self.nombre = nombre
        self.genero = genero
        self.crearTabla("Artistas", "Artista")
        self.datosEnLaTabla = []
        self.artistas = []

    def agregarDato(self):
        """
        Agrega un nuevo artista a la tabla de artistas en la base de datos.
        """
        query = "SELECT MAX(ID) FROM Artistas"
        self.apuntador.execute(query)
        max_id = self.apuntador.fetchone()[0] or 0

        datos = [
            (max_id + 1, self.nombre, self.genero)
        ]
        self.insertarDato("artistas", 3, datos)
        

    def obtenerDatoPorID(self, id_artista):
        """
        Obtiene los datos de un artista por su ID.
        """
        query = "SELECT * FROM artistas WHERE ID = ?"
        self.apuntador.execute(query, (id_artista,))
        datos = self.apuntador.fetchone()
        return datos
    
    def verTabla(self):
        """
        Agrega un nuevo artista a la base de datos con el nombre y género especificados.
        """
        query = "SELECT * FROM artistas"
        self.apuntador.execute(query)
        self.datosEnLaTabla = self.apuntador.fetchall()

    def ajustarIDs(self, id_eliminado):
        """
        Ajusta los IDs de los artistas después de eliminar uno.
        """
        query = "UPDATE artistas SET ID = ID - 1 WHERE ID > ?"
        self.apuntador.execute(query, (id_eliminado,))
        self.baseDeDatos.commit()

    def editarDato(self, id_artista, nuevo_nombre, nuevo_genero):
        """
        Edita los datos de un artista en la tabla.
        """
        queryEditarDatos = f"UPDATE artistas SET name = ?, genero = ? WHERE id = ?"
        self.apuntador.execute(queryEditarDatos, (nuevo_nombre, nuevo_genero, id_artista))
        self.baseDeDatos.commit()

    def eliminarDato(self, id_artista):
        """
        Elimina un artista de la tabla de artistas por su ID.
        """
        queryEliminarDatos = "DELETE FROM artistas WHERE id = ?"
        self.apuntador.execute(queryEliminarDatos, (id_artista,))
        self.baseDeDatos.commit()


