import sqlite3
import json

class Conexion:
    def __init__(self):
        self.baseDeDatos = sqlite3.connect("./3erEntregable/src/canciones.sqlite")
        self.apuntador = self.baseDeDatos.cursor()
        with open("C:/Taller_Programacion1/daniel_hernandez82222/3erEntregable/src/Queries.json", "r") as queries:
            self.query = json.load(queries)
        
        self.apuntador.execute("""
        CREATE TABLE IF NOT EXISTS artistas (
                id INTEGER PRIMARY KEY,
                name TEXT NOT NULL,
                genero TEXT NOT NULL
            )
        """)

    def crearTabla(self, nombre, columnas):
        queryCrearTabla = self.query["CrearTabla"].format(nombre, self.query[columnas])
        self.apuntador.execute(queryCrearTabla)
        self.baseDeDatos.commit()

    def insertarDato(self, tabla, columnas, datos):
        queryInsertarDatos = self.query["InsertarDatos"].format(tabla, ', '.join(['?'] * columnas))
        self.apuntador.executemany(queryInsertarDatos, datos)
        self.baseDeDatos.commit()

    def seleccionarTabla(self, tabla):
        querySeleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.apuntador.execute(querySeleccionarTabla)
        contenido = self.apuntador.fetchall()
        return contenido

    def actualizarDatos(self, tabla, columna, nuevovalor, id):
        queryActualizarDatos = f"UPDATE {tabla} SET {columna} = ? WHERE ID = ?"
        self.apuntador.execute(queryActualizarDatos, (nuevovalor, id))
        self.baseDeDatos.commit()

    def eliminarDatos(self, tabla, id):
        queryEliminarDatos = self.query["EliminarDatos"].format(tabla, 'ID')
        self.apuntador.execute(queryEliminarDatos, (id,))
        self.baseDeDatos.commit()
        
    def editarDatos(self, tabla, id_dato, nuevo_nombre, nuevo_genero):
        """
        Edita los datos de un artista en la tabla.
        """
        queryEditarDatos = f"UPDATE {tabla} SET Nombre = ?, Genero = ? WHERE ID = ?"
        self.apuntador.execute(queryEditarDatos, (nuevo_nombre, nuevo_genero, id_dato))
        self.baseDeDatos.commit()




