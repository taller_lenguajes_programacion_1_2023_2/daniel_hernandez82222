from Conexion import Conexion
import sqlite3
class Cancion(Conexion):
    """
    Clase que gestiona las operaciones relacionadas con la entidad de canciones en la base de datos SQLite.

    Esta clase hereda de la clase `Conexion` y proporciona métodos específicos para interactuar con la tabla
    de canciones en la base de datos.

    Atributos:
    - titulo (str): El título de la canción.
    - artista (str): El nombre del artista de la canción.
    - ID_Artista (int): El ID del artista relacionado con la canción.
    - datosEnLaTabla (list): Lista para almacenar los datos de la tabla de canciones.

    Métodos:
    - agregarDato(): Agrega una nueva canción a la tabla de canciones en la base de datos.
    - obtenerDatoPorID(id_cancion): Obtiene los datos de una canción por su ID.
    - ajustarIDs(id_eliminado): Ajusta los IDs de las canciones después de eliminar una canción.
    - verTabla(): Obtiene todos los datos de la tabla de canciones.
    - eliminarDato(id_cancion): Elimina una canción de la tabla de canciones.

    Uso:
    Para usar esta clase, primero instancia un objeto de `Cancion` y proporciona el título, artista y
    el ID del artista si es necesario. Luego, utiliza los métodos proporcionados para interactuar con
    la tabla de canciones en la base de datos SQLite.
    """
    def __init__(self, titulo="", artista="", ID_Artista=""):
        """
        Inicializa una instancia de la clase `Cancion` con los atributos especificados.

        Args:
        - titulo (str): El título de la canción (opcional).
        - artista (str): El nombre del artista de la canción (opcional).
        - ID_Artista (int): El ID del artista relacionado con la canción (opcional).

        Esta clase hereda de la clase `Conexion` para gestionar la conexión a la base de datos.
        Crea la tabla 'Canciones' si no existe y inicializa la lista de datos de la tabla.
        """
        super().__init__()
        self.titulo = titulo
        self.artista = artista
        self.ID_Artista = ID_Artista
        self.crearTabla("Canciones", "Cancion")
        self.datosEnLaTabla = []

    def agregarDato(self):
        """
        Agrega una nueva canción a la tabla de canciones en la base de datos.

        Calcula el ID máximo actual en la tabla, incrementa en uno y agrega la nueva canción con los datos
        especificados (ID, título, artista y ID del artista) a la base de datos.
        """
        query = "SELECT MAX(ID) FROM Canciones"
        self.apuntador.execute(query)
        max_id = self.apuntador.fetchone()[0] or 0

        datos = [
            (max_id + 1, self.titulo, self.artista, self.ID_Artista)
        ]
        self.insertarDato("Canciones", 4, datos)
        
    def agregarCancion(self, nombre, genero, id_artista):
        try:
            # Conectarse a la base de datos
            conn = sqlite3.connect('mi_base_de_datos.db')
            cursor = conn.cursor()

            # Insertar los datos de la canción en la tabla de canciones
            cursor.execute("INSERT INTO canciones (nombre, genero, id_artista) VALUES (?, ?, ?)", (nombre, genero, id_artista))

            # Guardar los cambios y cerrar la conexión
            conn.commit()
            conn.close()
            return True  # Devolver True si la inserción fue exitosa

        except Exception as e:
            print("Error al agregar la canción:", e)
            return False  # Devolver False si hubo un error en la inserción

        
    def obtenerDatoPorID(self, id_cancion):
        """
        Obtiene los datos de una canción por su ID.

        Args:
        - id_cancion (int): El ID de la canción que se desea obtener.

        Returns:
        - tuple: Una tupla que contiene los datos de la canción (ID, título, artista, ID del artista).
                 Devuelve None si no se encuentra la canción.
        """
        query = "SELECT * FROM Canciones WHERE ID = ?"
        self.apuntador.execute(query, (id_cancion,))
        datos = self.apuntador.fetchone()
        return datos
    
    def ajustarIDs(self, id_eliminado):
        """
        Ajusta los IDs de las canciones después de eliminar una canción.

        Args:
        - id_eliminado (int): El ID de la canción eliminada.

        Después de eliminar una canción, se actualizan los IDs de las canciones restantes en la base de datos
        para mantener una secuencia continua de IDs.
        """
        query = "UPDATE Canciones SET ID = ID - 1 WHERE ID > ?"
        self.apuntador.execute(query, (id_eliminado,))
        self.baseDeDatos.commit()

    def verTabla(self):
        """
        Obtiene todos los datos de la tabla de canciones y los almacena en la lista `datosEnLaTabla`.
        """
        query = "SELECT * FROM Canciones"
        self.apuntador.execute(query)
        self.datosEnLaTabla = self.apuntador.fetchall()

    def eliminarDato(self, id_cancion):
        """
        Elimina una canción de la tabla de canciones por su ID.

        Args:
        - id_cancion (int): El ID de la canción que se desea eliminar.

        Elimina la canción de la base de datos y ajusta los IDs de las canciones restantes.
        """
        super().eliminarDatos("Canciones", id_cancion)

        query = "UPDATE Canciones SET ID = ID - 1 WHERE ID > ?"
        self.apuntador.execute(query, (id_cancion,))
        self.baseDeDatos.commit()


