import sqlite3
import json

from hotel import Hotel
from habitacion import Habitacion
from cliente import Cliente
from conexion import Conexion

# Rutas de acceso
ruta_json = "C:\\Taller_Programacion1\\daniel_hernandez82222\\src\\PARCIAL 1\\static\\querys.json"
ruta_db = "C:\\Taller_Programacion1\\daniel_hernandez82222\\src\\PARCIAL 1\\static\\db_hotel.sqlite"

# Conectarse a la base de datos
conexion = Conexion(ruta_db)

# Crear las tablas
conexion.conectar()
conexion.crear("hoteles", {"id": "INTEGER PRIMARY KEY AUTOINCREMENT", "nombre": "VARCHAR(255)", "direccion": "VARCHAR(255)", "estrellas": "INTEGER", "servicios": "VARCHAR(255)", "numeroHabitaciones": "INTEGER"})
conexion.crear("habitaciones", {"id": "INTEGER PRIMARY KEY AUTOINCREMENT", "numero": "INTEGER", "tipo": "VARCHAR(255)", "capacidad": "INTEGER", "precioNoche": "FLOAT", "amenities": "VARCHAR(255)", "vista": "VARCHAR(255)", "hotel_id": "INTEGER", "FOREIGN KEY(hotel_id) REFERENCES hoteles(id)"})
conexion.crear("clientes", {"id": "INTEGER PRIMARY KEY AUTOINCREMENT", "nombre": "VARCHAR(255)", "apellido": "VARCHAR(255)", "dni": "VARCHAR(9)", "telefono": "VARCHAR(12)", "direccion": "VARCHAR(255)", "correoElectronico": "VARCHAR(255)"})

# Cargar datos de un archivo JSON
with open(ruta_json, "r") as f:
    data = json.load(f)
    for row in data:
        conexion.insertar("hoteles", row)
        for habitacion in row["habitaciones"]:
            conexion.insertar("habitaciones", habitacion)
        for cliente in row["clientes"]:
            conexion.insertar("clientes", cliente)

# Desconectarse de la base de datos
conexion.desconectar()

# Crear un hotel
hotel = Hotel("Hotel Las Vegas", "Calle Mayor, 10, Madrid", 5, ["piscina", "gimnasio", "spa"], 100)

# Crear una habitación
habitacion = Habitacion(101, "Doble", 2, 100, ["TV", "Wifi", "Climatizador"], "Vista a la ciudad")

# Crear un cliente
cliente = Cliente("Juan", "Pérez", "12345678A", "666666666", "Calle Ejemplo, 10, Madrid", "juan.perez@gmail.com")

# Reservar una habitación
reserva = Reserva(date(2023, 7, 20), date(2023, 7, 25), cliente, hotel, 500)

# Imprimir la información de la habitación
print(habitacion)

# Imprimir la información del hotel
print(hotel)

# Imprimir la información del cliente
print(cliente)

# Imprimir la información de la reserva
print(reserva)


