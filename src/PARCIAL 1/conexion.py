import sqlite3


class Conexion:
    """
    Clase que representa una conexión a una base de datos SQLite.

    Attributes:
        db_file (str): Ruta del archivo de la base de datos.
        connection (sqlite3.Connection): Conexión a la base de datos.

    Methods:
        conectar(): Conecta a la base de datos.
        desconectar(): Desconecta de la base de datos.
        insertar(tabla, datos): Inserta una fila en la tabla.
        eliminar(tabla, condiciones): Elimina filas de la tabla que cumplen las condiciones.
        actualizar(tabla, datos, condiciones): Actualiza filas de la tabla que cumplen las condiciones.
        seleccionar(tabla, condiciones): Devuelve un conjunto de filas de la tabla que cumplen las condiciones.
    """

    def __init__(self, db_file):
        self.db_file = db_file
        self.connection = None

    def conectar(self):
        """
        Conecta a la base de datos.

        Raises:
            Exception: Si no se puede conectar a la base de datos.
        """
        try:
            self.connection = sqlite3.connect(self.db_file)
        except sqlite3.Error as e:
            raise Exception(f"No se puede conectar a la base de datos: {e}")

    def desconectar(self):
        """
        Desconecta de la base de datos.
        """
        if self.connection:
            self.connection.close()
            self.connection = None

    def insertar(self, tabla, datos):
        """
        Inserta una fila en la tabla.

        Args:
            tabla (str): Nombre de la tabla.
            datos (dict): Datos de la fila a insertar.

        Raises:
            Exception: Si no se puede insertar la fila.
        """
        try:
            cursor = self.connection.cursor()
            cursor.execute(f"INSERT INTO {tabla} VALUES ({','.join(datos.keys())})", datos.values())
            self.connection.commit()
        except sqlite3.Error as e:
            raise Exception(f"No se puede insertar la fila: {e}")

    def eliminar(self, tabla, condiciones):
        """
        Elimina filas de la tabla que cumplen las condiciones.

        Args:
            tabla (str): Nombre de la tabla.
            condiciones (str): Condiciones de las filas a eliminar.

        Raises:
            Exception: Si no se pueden eliminar las filas.
        """
        try:
            cursor = self.connection.cursor()
            cursor.execute(f"DELETE FROM {tabla} WHERE {condiciones}")
            self.connection.commit()
        except sqlite3.Error as e:
            raise Exception(f"No se pueden eliminar las filas: {e}")

    def actualizar(self, tabla, datos, condiciones):
        """
        Actualiza filas de la tabla que cumplen las condiciones.

        Args:
            tabla (str): Nombre de la tabla.
            datos (dict): Datos a actualizar.
            condiciones (str): Condiciones de las filas a actualizar.

        Raises:
            Exception: Si no se pueden actualizar las filas.
        """
        try:
            cursor = self.connection.cursor()
            cursor.execute(
                f"UPDATE {tabla} SET {','.join([f'{key} = {value}' for key, value in datos.items()])} WHERE {condiciones}"
            )
            self.connection.commit()
        except sqlite3.Error as e:
            raise Exception(f"No se pueden actualizar las filas: {e}")

    def seleccionar(self, tabla, condiciones):
        """
        Devuelve un conjunto de filas de la tabla que cumplen las condiciones.

        Args:
            tabla (str): Nombre de la tabla.
            condiciones (str): Condiciones de las filas a seleccionar.

        Returns:
            list: Conjunto de filas de la tabla que cumplen las condiciones.
        """
        try:
            cursor = self.connection.cursor()
            cursor.execute(f"SELECT * FROM {tabla} WHERE {condiciones}")
            return cursor.fetchall()
        except sqlite3.Error as e:
            raise Exception(f"No se pueden seleccionar las filas: {e}")



