# archivo habitacion.py

class Habitacion:
    def __init__(self, numero: int, tipo: str, capacidad: int, precioNoche: float, amenities: list[str], vista: str):
        self.id = id(self)
        self.numero = numero
        self.tipo = tipo
        self.capacidad = capacidad
        self.precioNoche = precioNoche
        self.amenities = amenities
        self.vista = vista

class HabitacionDoble(Habitacion):

    def __init__(self, numero: int, capacidad: int, precioNoche: float, amenities: list[str], vista: str):
        super().__init__(numero, "Doble", capacidad, precioNoche, amenities, vista)

    @property
    def numero(self):
        return self._numero

    @numero.setter
    def numero(self, numero):
        self._numero = numero

    @property
    def capacidad(self):
        return self._capacidad

    @capacidad.setter
    def capacidad(self, capacidad):
        self._capacidad = capacidad

    @property
    def precioNoche(self):
        return self._precioNoche

    @precioNoche.setter
    def precioNoche(self, precioNoche):
        self._precioNoche = precioNoche

    @property
    def amenities(self):
        return self._amenities

    @amenities.setter
    def amenities(self, amenities):
        self._amenities = amenities

    @property
    def vista(self):
        return self._vista

    @vista.setter
    def vista(self, vista):
        self._vista = vista

    def __str__(self):
        return f"Habitacion doble {self._numero} ({self._capacidad} personas)"



