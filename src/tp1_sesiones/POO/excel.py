import pandas as pd

class Excel:
    def __init__(self,nombre_xlsx= "") -> None:
        self.___nombre_xlsx = nombre_xlsx
        # C:\Taller_Programacion1\daniel_hernandez82222\src\tp1_sesiones\POO\static\xlsx\hoja_vida.xlsx
        self.__ruta_xlsx = "./src/tp1_sesiones/poo/static/xlsx{}".format(self.___nombre_xlsx)
        
    
    def leer_xlsx(self, nom_hoja=""):
        if nom_hoja == "":
            df = pd.read_excel(self.__ruta_xlsx)
        else:
            df = pd.read_excel(self.__ruta_xlsx,sheet_name=nom_hoja)
        return df
    