
import json
import sqlite3


class Conexion:
    def __init__(self) :
        __nom_db = "db_poli.sqlite"
        self.__querys = self.obtener_json()
        self.__conx = sqlite3.connect("./src/tp1_sesiones/poo/static/db/{}".format(__nom_db))
        self.__cursor = self.__conx.cursor()
        
        
    def obtener_json(self):
       # C:\Taller_Programacion1\daniel_hernandez82222\src\tp1_sesiones\POO\static\json
        ruta = "./src/tp1_sesiones/poo/static/json/querys.json"
        querys = {}
        with open(ruta,'r') as file_json:
            querys = json.load(file_json)
        return querys
    
    def crear_tabla(self,nom_tabla = "", nom_json = ""):
        if nom_tabla != "" and nom_json != "":
            columns = self.__querys[nom_json]
            query = self.__querys["crear_tabla"].format(nom_tabla,columns)
            self.__cursor.execute(query)
            self.__conx.commit()
            return True
        return False
            
            
        
        
    
# conx = Conexion()
# print(conx)