## ***** Taller de Repaso ******

#  1. Imprimir "Hola, mundo!"
print("Hola, mundo!")

# 2.Variables y asignación.
nombre = "Daniel Hernandez Rico" 
print((nombre))

entero = 19 
print((entero))

flotante = 19.5 
print((flotante))

booleano = True 
print((booleano))

# 3. Tipos de datos básicos: int, float, string.
entero = 5
decimal = 2.5
string = "Hola"

# 4. Operaciones aritméticas básicas.
suma = entero + decimal
resta = entero - decimal
multiplicacion = entero * decimal
division = entero / decimal
print("suma:", suma)
print("resta:", resta)
print("multiplicación:", multiplicacion)
print("división:", division)

# 5. Conversión entre tipos de datos.
numero_como_texto = "20"
numero_convertido = int(numero_como_texto)
decimal_convertido = float(entero)
print(numero_como_texto, numero_convertido, decimal_convertido)

# 6. Solicitar entrada del usuario.
nombre = input("Por favor, ingresa tu nombre: ")
edad = int(input("Ingresa tu edad: "))
print("Nombre:", nombre, ", Edad:", edad, "años.")

# 7. Condicional if.
numero = 11
if numero > 0:
    print("El número es positivo.")

# 8. Condicional if-else.
if numero % 2 == 0:
    print("El número es par.")
else:
    print("El número es impar.")

# 9. Condicional if-elif-else.
if numero > 0:
    print("El número es positivo.")
elif numero < 0:
    print("El número es negativo.")
else:
    print("El número es cero.")

# 10. Bucle for.
for i in range(5):
    print("Iteración", i)

# 11. Bucle while.
contador = 0
while contador < 5:
    print("Contador:", contador)
    contador += 1

# 12. Uso de break y continue.
for i in range(10):
    if i == 3:
        continue
    if i == 8:
        break
    print("Valor:", i)

# 13. Listas y sus operaciones básicas.
lista = [1, 2, 3, 4, 5]
print("Lista:", lista)
print("Longitud de la lista:", len(lista))
print("Primer elemento:", lista[0])
print("Último elemento:", lista[-1])

# 14. Tuplas y su inmutabilidad.
tupla = (1, 2, 3)
print("Tupla:", tupla)
# modificar una tupla causa un error, por lo que no hay operaciones de modificación.

# 15. Conjuntos y operaciones.
conjunto1 = {1, 2, 3}
conjunto2 = {3, 4, 5}
print("Unión de conjuntos:", conjunto1.union(conjunto2))
print("Intersección de conjuntos:", conjunto1.intersection(conjunto2))

# 16. Diccionarios y operaciones clave-valor.
diccionario = {"nombre": "Juan", "edad": 25, "ciudad": "México"}
print("Diccionario:", diccionario)
print("Valor de 'nombre':", diccionario["nombre"])
print("Valor de 'edad':", diccionario["edad"])

# 17. List funciones básicas
lista = [1, 2, 3, 4, 5]
print("Lista:", lista)
lista.append(6)
print("Lista después de append:", lista)
lista.pop()
print("Lista después de pop:", lista)
print("Índice de '3':", lista.index(3))

# 18. Leer un archivo de texto.
with open ("./src/tp1_sesiones/static/txt/Practica1.txt", "r") as archivo:
 print("Contenido del archivo:\n", archivo.read())

# 19. Escribir en un archivo de texto.
with open ("./src/tp1_sesiones/static/txt/Practica1_2.txt", "w") as archivo:
 archivo.write("1;9/08/2023;Introducción a archivos planos;Presentación de la unidad y ejemplos básicos;Lectura recomendada sobre archivos en Python")

# 20. Modos de apertura: r, w, a, rb, wb.
## ** r: Leer; w: Escribir; a: Agregar; rb: Leer Imagenes; wb; Escribir Imagenes. **
archivo_binario_lectura = open("./src/tp1_sesiones/static/jpg/imagen.png", "rb")
contenido_binario = archivo_binario_lectura.read()
print("Contenido binario del archivo (modo lectura binaria):", contenido_binario[:20])

archivo_binario_escritura = open("./src/tp1_sesiones/static/jpg/imagen_2.jpg.png", "wb")
contenido_binario_2 = b'/x89PNG/r/n/x1a/n/x00/x00/x00/rIHDR/x00/x00/x02'

# 21. Trabajar con archivos JSON.
import json
datos_1 = {
    "nombre": "Juan",
    "edad": 30,
    "ciudad": "México"
}

# Escribir datos en un archivo JSON.
with open("datos_1.json", "w") as archivo_json:
    json.dump(datos_1, archivo_json)

print("Datos escritos en archivo JSON.")

# Leer datos de un archivo JSON.
with open("datos_1.json", "r") as archivo_json:
    datos_leidos = json.load(archivo_json)

print("Datos leídos desde el archivo JSON:")
print("Nombre:", datos_leidos["nombre"])
print("Edad:", datos_leidos["edad"])
print("Ciudad:", datos_leidos["ciudad"])


#*******************************************************************************************************
# ***** Crear un DataFrame *****
import pandas as pd
datos_2 = {
    "nombre": ["Ana", "Juan", "María", "Luis"],
    "edad": [25, 30, 28, 22],
    "ciudad": ["México", "Madrid", "Bogotá", "Lima"]
}
df = pd.DataFrame(datos_2)

# 22. Leer un archivo CSV.
df_csv = pd.read_csv("./src/tp1_sesiones/datos_2.csv")

# 23. Filtrar datos en un DataFrame.
filtrado = df[df["edad"] > 25]

# 24. Operaciones básicas: sum(), mean(), max(), min().
suma_edades = df["edad"].sum()
promedio_edades = df["edad"].mean()
max_edad = df["edad"].max()
min_edad = df["edad"].min()

# 25. Uso de iloc y loc.
fila_1 = df.iloc[0]
datos_juan = df.loc[df["nombre"] == "Juan"]

# 26. Agrupar datos con groupby.
agrupado_ciudad = df.groupby("ciudad")["edad"].mean()

# 27. Unir DataFrames con merge y concat.
df1 = pd.DataFrame({"A": [1, 2, 3], "B": [4, 5, 6]})
df2 = pd.DataFrame({"A": [7, 8, 9], "B": [10, 11, 12]})
merged = pd.merge(df1, df2, on="A")
concatenated = pd.concat([df1, df2])

# 28. Manipular series temporales.
fechas = pd.date_range(start="2023-01-01", periods=5, freq="D")
df_fechas = pd.DataFrame({"Fecha": fechas, "Valores": [10, 20, 30, 40, 50]})
df_fechas.set_index("Fecha", inplace=True)

# 29. Exportar un DataFrame a CSV.
df.to_csv("./src/tp1_sesiones/df_exportado.csv", index=False)

# 30. Convertir un DataFrame a JSON.
df_json = df.to_json("./src/tp1_sesiones/df_exportado.json", orient="records")

print("DataFrame creado:\n", df)
print("DataFrame desde CSV:\n", df_csv)
print("Filtrado:\n", filtrado)
print("Suma de edades:", suma_edades)
print("Promedio de edades:", promedio_edades)
print("Máxima edad:", max_edad)
print("Mínima edad:", min_edad)
print("Fila 1:\n", fila_1)
print("Datos de Juan:\n", datos_juan)
print("Agrupado por ciudad:\n", agrupado_ciudad)
print("Merged:\n", merged)
print("Concatenated:\n", concatenated)
print("DataFrame con fechas:\n", df_fechas)
print("DataFrame exportado a CSV y JSON.")
      
#                             ##########################################################                              #
# 1. Leer una tabla HTML desde la página "https://www.marca.com/motor/motogp/clasificacion-motogp.html" y guardarla en un DataFrame.
url = "https://www.marca.com/motor/motogp/clasificacion-motogp.html"
dfs = pd.read_html(url)
df_excel = dfs[0]
columns = ["Posicion","Piloto","Total","GP Portugal","GP Portugal","GP Portugal","GP Portugal","GP Portugal","GP Portugal","GP Portugal","GP Portugal","GP Portugal","GP Portugal"]

# 2. Guardar la tabla de HTML en un archivo Excel.
df_excel.to_excel('src/tp1_sesiones/static/xlsx/tablas_sesion7.xlsx', index=False)

# 3. Exportar los primeros 10 registros del DataFrame anterior a un archivo CSV "primeros10.csv".
df_excel.head(10).to_csv("src/tp1_sesiones/static/xlsx/primerosqo.csv")

# 4. Leer tres tablas HTML y guárdalas en tres hojas diferentes de un archivo Excel "tres_tablas.xlsx".
url1 = "https://www.marca.com/motor/motogp/clasificacion-motogp.html"
url2 = "https://www.espn.com.co/deporte-motor/f1/posiciones"
url3 = "https://datosmacro.expansion.com/pib"
dfs = pd.read_html(url1), pd.read_html(url2), pd.read_html(url3)
with pd.ExcelWriter('src/tp1_sesiones/static/xlsx/tablas_sesion7(2).xlsx') as writer:
    for i, df in enumerate(dfs):
        df[0].to_excel(writer, sheet_name=f'Tabla_{i+1}', index=False)
# Hoja1=pd.read_html("https://www.marca.com/motor/motogp/clasificacion-motogp.html")[0]
# Hoja2=pd.read_html("https://www.espn.com.co/deporte-motor/f1/posiciones")[0]
# Hoja3=pd.read_html("https://datosmacro.expansion.com/pib")[0]
# Hoja2=Hoja2.tail(5)
# Hoja3=Hoja3.head(5)
# with pd.ExcelWriter("src/tp1_sesiones/static/xlsx/tablas_sesion7(2).xlsx") as writer:
#     Hoja1.to_excel(writer,sheet_name="Hoja1")
#     Hoja2.to_excel(writer,sheet_name="Hoja2")
#     Hoja3.to_excel(writer,sheet_name="Hoja3")

# 5. Leer un archivo Excel, filtrar registros donde la columna "edad" sea mayor a 30 y guardarlo en "mayores30.csv".
df_excel = pd.read_excel('src/tp1_sesiones/static/xlsx/clase_4_dataset.xlsx', engine='openpyxl')
mayores_30 = df_excel[df_excel["Edad"] > 30]
mayores_30.to_csv('src/tp1_sesiones/static/xlsx/mayores30.csv', index=False)

# 6. Leer un archivo CSV, seleccionar solo las columnas "nombre" y "profesion" y guardarlo en un archivo Excel "nombres_ciudades.xlsx".
Archivo=pd.read_excel("src/tp1_sesiones/static/xlsx/clase_4_dataset.xlsx")
Columns = ['Nombre', 'Profesión']
Archivo[Columns].to_excel("src/tp1_sesiones/static/xlsx/tabla_sesion7(3).xlsx")

# 7. Leer la primera y tercera hoja del archivo Excel "varias_hojas.xlsx" y combinarlas en un DataFrame.
xlsx = pd.ExcelFile('src/tp1_sesiones/static/xlsx/varias_hojas.xlsx', engine='openpyxl')
df_hoja1 = pd.read_excel(xlsx, sheet_name='Hoja1')
df_hoja3 = pd.read_excel(xlsx, sheet_name='Hoja3')
df_combinado = pd.concat([df_hoja1, df_hoja3], ignore_index=True)
