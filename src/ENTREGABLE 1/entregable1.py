# ****Daniel Hernandez Rico ****
# Cedula: 1038866458
# Puntos: {8, 18, 28, 38, 48, 58, 68, 78, 88, 98}
# variables: dhr_int1


#8. Verificar si la variable s es de tipo String o no:
print("# 8.")
dhr_s = "Hola"
if type(dhr_s) is str:
    print("La variable 'dhr_s' es string.")
else:
    print("La variable 'dhr_s' no es string. \n")
    
#18. Escribe un programa que tome una cadena s, y la devuelva invertida:
print("# 18.")
dhr_s = input("Ingresa una cadena: ")
cadena_invertida = dhr_s[::-1]
print("Cadena invertida: ", cadena_invertida)

#28. Escribe un programa que tome dos listas y devuelva true si tienen al menos un miembro en comun:
print("\n# 28.")
dhr_lista_1=(1,2,3,4,5)
dhr_lista_2=(2,6,8,10)
dhr_tienen_miembro_comun = any(item in dhr_lista_2 for item in dhr_lista_1)
print(dhr_tienen_miembro_comun) 

#38. Escribe un programa que sume todos los valores de un diccionario:
print("\n# 38")
diccionario = {
    'dhr_a': 1,
    'dhr_b': 5,
    'dhr_c': 3,
    'dhr_d': 2
}
dhr_suma = 0
for valor in diccionario.values():
    dhr_suma += valor
print("La suma de los valores del diccionario es:", dhr_suma)

#48. Escribe un programa que combine dos archivos csv en uno solo:
print("\n# 48.")
import pandas as pd

dhr_archivo1 = 'src/ENTREGABLE 1/archivos/mayores30.csv'
dhr_archivo2 = 'src/ENTREGABLE 1/archivos/primeros10.csv'
dhr_archivo_salida = 'src/ENTREGABLE 1/archivos/archivo_combinado.csv'

dhr_df1 = pd.read_csv(dhr_archivo1)
dhr_df2 = pd.read_csv(dhr_archivo2)

dhr_df_combinado = pd.concat([dhr_df1, dhr_df2], ignore_index=True)
dhr_df_combinado.to_csv(dhr_archivo_salida, index=False)

print("Archivos combinados en", dhr_archivo_salida)

#58. Escribe un programa que convierta un archivo CSV a JSON:
print("\n# 58.")
import csv
import json
dhr_archivo_csv = 'src/ENTREGABLE 1/archivos/mayores30.csv'
dhr_archivo_json = 'src/ENTREGABLE 1/archivos/datos.json'

dhr_datos = []
with open(dhr_archivo_csv, 'r', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        dhr_datos.append(row)

with open(dhr_archivo_json, 'w') as jsonfile:
    json.dump(dhr_datos, jsonfile, indent=4)

print(f"Archivo JSON '{dhr_archivo_json}' generado.")

#68. Simula un archivo Excel, Alumnos.xlsx, escribe un programa que ordene los datos por el promedio de un alumno:
print("\n# 68.")
dhr_data = {
    'Nombre': ['Alumno1', 'Alumno2', 'Alumno3', 'Alumno4'],
    'Edad': [20, 22, 21, 23],
    'Promedio': [4.0, 3.6, 4.5, 3.2]
}

dhr_df = pd.DataFrame(dhr_data)

dhr_df = dhr_df.sort_values(by='Promedio', ascending=False)
dhr_df.to_excel('src/ENTREGABLE 1/archivos/Alumnos_Ordenados.xlsx', index=False)

print("Datos de alumnos ordenados por promedio y guardados en 'Alumnos_Ordenados.xlsx'.")

#78. Simula un archivo Excel, escribe un programa que muestre un resumen estadistico de una columna numerica:
print("\n# 78.")
import pandas as pd
import numpy as np

data = {
    'Columna_numerica': np.random.rand(100) * 100  # Datos aleatorios entre 0 y 100
}

df = pd.DataFrame(data)

resumen_estadistico = df['Columna_numerica'].describe()

print("Resumen estadístico de la columna numérica:")
print(resumen_estadistico)

#88. Escribe un prograsma que lea un archivo html, y convierta una coumna de precios string con simbolo de moneda a float:
print("\n# 88.")
import pandas as pd
from bs4 import BeautifulSoup
import pandas as pd
import requests
from bs4 import BeautifulSoup


url = 'https://www.indexmundi.com/es/precios-de-mercado/?mercancia=gasolina&meses=300&moneda=cop'

response = requests.get(url)

if response.status_code == 200:
    soup = BeautifulSoup(response.text, 'html.parser')
    tabla_html = soup.find('table')

    df = pd.read_html(str(tabla_html))[1]
    df.columns = ["Mes","Precio","Tasa Cambio"]
    def convertir_a_float(cadena):
        cadena_sin_moneda = cadena.replace('$', '').replace(',', '').strip()
        return float(cadena_sin_moneda)

    df['Precio'] = df['Precio'].apply(convertir_a_float)
    
    print(df)
else:
    print(f'Error al obtener la página web. Código de respuesta: {response.status_code}')
    
#98. Simula un archivo excel con una columna de texto, escribe un programa que utilice expresiones regulares para extraer y contar palabras especificas:
print("\n# 98.")  
import pandas as pd
import re

df = pd.read_csv('src/ENTREGABLE 1/archivos/archivo_excel.csv')

def contar_palabras_especificas(dhr_texto, dhr_palabras_clave):
    dhr_contador = 0
    for dhr_palabra_clave in dhr_palabras_clave:
        dhr_coincidencias = re.findall(r'\b' + dhr_palabra_clave + r'\b', dhr_texto, flags=re.IGNORECASE)
        dhr_contador += len(dhr_coincidencias)
    return dhr_contador

dhr_palabras_clave = ['ejemplo', 'palabras', 'otras']

df['Conteo'] = df['Texto'].apply(lambda x: contar_palabras_especificas(x, dhr_palabras_clave))

print(df)



