import pandas as pd
import sqlite3
import os

### 1. Creación del archivo Excel: (10%) ###
### 2. Creación de las clases en Python: (20%) ###

# Rutas
excel_file = r'C:\Taller_Programacion1\daniel_hernandez82222\src\ENTREGABLE2\static\xlsx\animales.xlsx'
sqlite_db = r'C:\Taller_Programacion1\daniel_hernandez82222\src\ENTREGABLE2\static\db\db_ent2.sqlite'

## CLASE PADRE ##
class Animal:
    """
    Clase que representa a un animal.

    Attributos:
        id (int): El identificador único del animal.
        nombre (str): El nombre del animal.
        especie (str): La especie del animal.
        edad (int): La edad del animal.
        habitat (str): El hábitat del animal.
        dieta (str): La dieta del animal.
    """

    def __init__(self, id, nombre, especie, edad, habitat, dieta):
        self._id = id
        self._nombre = nombre
        self._especie = especie
        self._edad = edad
        self._habitat = habitat
        self._dieta = dieta

    def get_id(self):
        """
        Obtiene el identificador único del animal.

        Returns:
            int: El identificador único del animal.
        """
        return self._id

    def set_id(self, id):
        """
        Establece el identificador único del animal.

        Args:
            id (int): El nuevo identificador único del animal.
        """
        self._id = id

    def get_nombre(self):
        """
        Obtiene el nombre del animal.

        Returns:
            str: El nombre del animal.
        """
        return self._nombre

    def set_nombre(self, nombre):
        """
        Establece el nombre del animal.

        Args:
            nombre (str): El nuevo nombre del animal.
        """
        self._nombre = nombre

    def get_especie(self):
        return self._especie

    def set_especie(self, especie):
        self._especie = especie

    def get_edad(self):
        return self._edad

    def set_edad(self, edad):
        self._edad = edad

    def get_habitat(self):
        return self._habitat

    def set_habitat(self, habitat):
        self._habitat = habitat

    def get_dieta(self):
        return self._dieta

    def set_dieta(self, dieta):
        self._dieta = dieta
        
    def crear_en_db(self, conexion):
        """
        Crea una entrada del animal en la base de datos.

        Args:
            conexion (Conexion): La instancia de la conexión a la base de datos.

        Raises:
            sqlite3.Error: Si ocurre un error al ejecutar la consulta SQL.
        """
        query = "INSERT INTO Animal (id, nombre, especie, edad, habitat, dieta) VALUES (?, ?, ?, ?, ?, ?)"
        params = (self._id, self._nombre, self._especie, self._edad, self._habitat, self._dieta)
        conexion.ejecutar_sql(query, params)

    @staticmethod
    def leer_de_db(conexion):
        query = "SELECT * FROM Animal"
        cursor = conexion.ejecutar_sql(query)
        if cursor:
            return cursor.fetchall()
        return None

    def actualizar_en_db(self, conexion):
        query = "UPDATE Animal SET nombre=?, especie=?, edad=?, habitat=?, dieta=? WHERE id=?"
        params = (self._nombre, self._especie, self._edad, self._habitat, self._dieta, self._id)
        conexion.ejecutar_sql(query, params)

    def eliminar_de_db(self, conexion):
        query = "DELETE FROM Animal WHERE id=?"
        params = (self._id,)
        conexion.ejecutar_sql(query, params)

    @staticmethod
    def migrar_desde_excel(conexion, excel_file):
        """
        Lee datos de un archivo Excel y migra los registros a la base de datos.

        Args:
            conexion (Conexion): La instancia de la conexión a la base de datos.
            excel_file (str): La ruta del archivo Excel.

        Raises:
            sqlite3.Error: Si ocurre un error al ejecutar la consulta SQL.
        """
        df = pd.read_excel(excel_file, sheet_name="Clase Padre")
        for index, row in df.iterrows():
            animal = Animal(row['id'], row['nombre'], row['especie'], row['edad'], row['habitat'], row['dieta'])
            animal.crear_en_db(conexion)

## CLASE HIJO ##
class Mamifero(Animal):
    """
    Clase que representa a un mamífero, que es un tipo de animal.

    Attributes:
        gestacion (str): El período de gestación del mamífero.
        camadas_anuales (int): El número de camadas anuales del mamífero.
        esperanza_vida (int): La esperanza de vida del mamífero en años.
        territorio (str): El territorio del mamífero.
        tipo_piel (str): El tipo de piel del mamífero.
        alimentacion (str): La alimentación del mamífero.
    """

    def __init__(self, id, nombre, especie, edad, habitat, dieta, gestacion, camadas_anuales, esperanza_vida, territorio, tipo_piel, alimentacion):
        super().__init__(id, nombre, especie, edad, habitat, dieta)
        self._gestacion = gestacion
        self._camadas_anuales = camadas_anuales
        self._esperanza_vida = esperanza_vida
        self._territorio = territorio
        self._tipo_piel = tipo_piel
        self._alimentacion = alimentacion

     # getter y setter
    def get_gestacion(self):
        return self._gestacion

    def set_gestacion(self, gestacion):
        self._gestacion = gestacion

    def get_camadas_anuales(self):
        return self._camadas_anuales

    def set_camadas_anuales(self, camadas_anuales):
        self._camadas_anuales = camadas_anuales

    def get_esperanza_vida(self):
        return self._esperanza_vida

    def set_esperanza_vida(self, esperanza_vida):
        self._esperanza_vida = esperanza_vida

    def get_territorio(self):
        return self._territorio

    def set_territorio(self, territorio):
        self._territorio = territorio

    def get_tipo_piel(self):
        return self._tipo_piel

    def set_tipo_piel(self, tipo_piel):
        self._tipo_piel = tipo_piel

    def get_alimentacion(self):
        return self._alimentacion

    def set_alimentacion(self, alimentacion):
        self._alimentacion = alimentacion

    def crear_en_db(self, conexion):
        """
        Crea una entrada del mamífero en la base de datos.

        Args:
            conexion (Conexion): La instancia de la conexión a la base de datos.

        Raises:
            sqlite3.Error: Si ocurre un error al ejecutar la consulta SQL.
        """
        query = "INSERT INTO Mamifero (id, nombre, especie, edad, habitat, dieta, gestacion, camadas_anuales, esperanza_vida, territorio, tipo_piel, alimentacion) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        params = (self._id, self._nombre, self._especie, self._edad, self._habitat, self._dieta, self._gestacion, self._camadas_anuales, self._esperanza_vida, self._territorio, self._tipo_piel, self._alimentacion)
        conexion.ejecutar_sql(query, params)

    @staticmethod
    def leer_de_db(conexion):
        query = "SELECT * FROM Mamifero"
        cursor = conexion.ejecutar_sql(query)
        if cursor:
            return cursor.fetchall()
        return None
    
    def actualizar_en_db(self, conexion):
        query = "UPDATE Animal SET nombre=?, especie=?, edad=?, habitat=?, dieta=? WHERE id=?"
        params = (self._nombre, self._especie, self._edad, self._habitat, self._dieta, self._id)
        conexion.ejecutar_sql(query, params)

    def eliminar_de_db(self, conexion):
        query = "DELETE FROM Animal WHERE id=?"
        params = (self._id,)
        conexion.ejecutar_sql(query, params)

    @staticmethod
    def migrar_desde_excel(conexion, excel_file):
        """
        Lee datos de un archivo Excel y migra los registros de mamíferos a la base de datos.

        Args:
            conexion (Conexion): La instancia de la conexión a la base de datos.
            excel_file (str): La ruta del archivo Excel.

        Raises:
            sqlite3.Error: Si ocurre un error al ejecutar la consulta SQL.
        """
        df = pd.read_excel(excel_file, sheet_name="Clase Hijo")
        for index, row in df.iterrows():
                 mamifero = Mamifero(
                 row['id'], row['nombre'], row['especie'], row['edad'], row['habitat'], row['dieta'],
                 row['gestacion'], row['camadas_anuales'], row['esperanza_vida'], row['territorio'],
                 row['tipo_piel'], row['alimentacion']
            )
                 mamifero.crear_en_db(conexion)

### 3. Clase de conexión y funcionesCRUD(40%) ###
## CLASE CONEXION ##
class Conexion:
    """
    Clase que representa una conexión a una base de datos SQLite.

    Attributes:
        db_file (str): La ruta del archivo de la base de datos.
        connection (sqlite3.Connection): La conexión a la base de datos.
    """

    def __init__(self, db_file):
        self.db_file = db_file
        self.connection = None

    def conectar(self):
        """
        Conecta a la base de datos SQLite.

        Raises:
            sqlite3.Error: Si ocurre un error al conectar a la base de datos.
        """
        try:
            self.connection = sqlite3.connect(self.db_file)
            print("Conexión a la base de datos exitosa.")
        except sqlite3.Error as e:
            print(f"Error al conectar a la base de datos: {e}")

    def desconectar(self):
        """
        Desconecta de la base de datos SQLite.
        """
        if self.connection:
            self.connection.close()
            print("Desconexión de la base de datos exitosa.")

    def ejecutar_sql(self, query, params=None):
        """
        Ejecuta una consulta SQL en la base de datos.

        Args:
            query (str): La consulta SQL a ejecutar.
            params (tuple): Los parámetros de la consulta (opcional).

        Returns:
            sqlite3.Cursor: El cursor de la consulta.

        Raises:
            sqlite3.Error: Si ocurre un error al ejecutar la consulta SQL.
        """
        try:
            cursor = self.connection.cursor()
            if params:
                cursor.execute(query, params)
            else:
                cursor.execute(query)
            self.connection.commit()
            return cursor
        except sqlite3.Error as e:
            print(f"Error al ejecutar la consulta SQL: {e}")
            return None
### 4. mplementación(20%) ###
animal_df = pd.read_excel(excel_file, sheet_name='Clase Padre')
mamifero_df = pd.read_excel(excel_file, sheet_name='Clase Hijo')

conn = sqlite3.connect(sqlite_db)

animal_df.to_sql('Animal', conn, if_exists='replace', index=False)

mamifero_df.to_sql('Mamifero', conn, if_exists='replace', index=False)

conn.close()

print("Datos migrados exitosamente a SQLite.")

### 5. Documentación con Docstrings: (5%) ###
### 6. Diagrama de Clases: (5%) ###
