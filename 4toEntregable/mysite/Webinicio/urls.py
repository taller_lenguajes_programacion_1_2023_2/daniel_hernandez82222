from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.user_login_view, name='login'),
    path('registro/', views.register_view, name='register'),
    path('marketplace/', views.marketplace, name='marketplace'),
    path('marketplaceCategoria/', views.marketplaceCategoria, name='marketplaceCategoria'),
    path('detalles/', views.detalles, name='detalles'),  
]