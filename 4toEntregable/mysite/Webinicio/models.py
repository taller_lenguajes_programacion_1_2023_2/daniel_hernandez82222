from django.db import models

# Create your models here.
class Developer(models.Model):
    nameDev = models.CharField(max_length=200)
    cedulaDev = models.CharField(max_length=200)
    mailDev = models.EmailField(max_length=200)
    
    def __str__(self):
        return self.nameDev + ' - ' + self.cedulaDev + ' - ' + self.mailDev
    