function filterCategory(category) {
    let services = document.querySelectorAll('.service');
    services.forEach(service => {
        service.style.display = 'none';
        if (service.classList.contains(category)) {
            service.style.display = 'block';
        }
    });
}
document.addEventListener('DOMContentLoaded', function () {
    // Filtrar por "todos" al cargar la página
    filterCategory('todos');

    // Agregar un event listener a cada botón de categoría
    const categoryButtons = document.querySelectorAll('.navbar button');
    categoryButtons.forEach(button => {
        button.addEventListener('click', function () {
            const category = this.getAttribute('data-category');
            filterCategory(category);
        });
    });
});