from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('login/', views.login),
    path('registro/', views.registro),
    path('marketplace/', views.marketplace, name='marketplace'),
    path('marketplaceCategoria/', views.marketplaceCategoria),
    path('detalle/', views.detalle),
    path('webproductos/', include('Webproductos.urls')),
]